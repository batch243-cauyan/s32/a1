const http = require("http");
let welcome = {
    message:""
}

http.createServer(function (request, response) {

	if(request.url == "/" && request.method == "GET"){
        welcome.message = "Welcome to Booking System";
		
		response.writeHead(200,{'Content-Type': 'application/json'});


	    response.write((welcome.message));
		
	    response.end();

	}

    if(request.url == "/profile" && request.method == "GET"){
        welcome.message = "Welcome to your profile";
		
		response.writeHead(200,{'Content-Type': 'application/json'});


	    response.write((welcome.message));
	
	    response.end();

	}

    if(request.url == "/courses" && request.method == "GET"){
        welcome.message = "Here's our courses available";
		
		response.writeHead(200,{'Content-Type': 'application/json'});

 
	    response.write(welcome.message);
		
	    response.end();

	}



	if(request.url == "/addCourse" && request.method == "POST"){

	
		welcome.message = 'Add course to our resources';
        let dummyData = '';
	
	 	request.on('data', function(data){

	 		
	 		dummyData += data;

	 	});

	 	
	 	request.on('end', function(){

	    	response.writeHead(200,{'Content-Type': 'application/json'});
	    	response.write(welcome.message);
	    	response.end();
	  });
	}

}).listen(4000);


console.log('Server running at localhost:4000');